(ns clj.game-test
  (:require
    [clojure.test :refer :all]
    [tic-tac-toe.game :as g]
    [tic-tac-toe.board :refer [empty-board]])
  (:import (java.util UUID)))

(defonce ^:private state @#'g/state)

(def player1 [:x "player1uid"])
(def player2 [:o "player2uid"])
(def default-game-state {:board   empty-board
                         :player1 player1
                         :player2 player2
                         :turn    :x})

(defn- game-state-helper []
  (g/start!)
  (g/init-game! player1 player2))

(use-fixtures :each (fn [f] (f) (g/stop!)))

(deftest game-lifecycle
  (testing "Testing start! & stop!"
    (do (g/start!) (is (= {} @state) "Start initialises state atom."))
    (do (g/stop!) (is (nil? @state) "Stop resets state atom."))))

(deftest init-game
  (testing "Testing init-game!"
    (is (thrown? Exception (g/init-game! player1 player2))
        "Throws if service is not running.")

    (is (uuid? (-> (game-state-helper) str (subs 1) UUID/fromString))
        "Returns game uid on success.")))

(deftest get-accessor
  (testing "Testing get-game"
    (is (thrown? Exception (g/get-game :anything))
        "Throws if service is not running.")

    (is (= default-game-state (g/get-game (game-state-helper)))
        "Gets game-state map correctly.")

    (do (game-state-helper)
        (is (nil? (g/get-game :anything))
            "Returns nil when game-uid is not found."))))

(deftest player-accessor
  (testing "Testing game->players"
    (is (thrown? Exception (g/game->players :anything))
        "Throws if service is not running.")

    (is (= [player1 player2] (g/game->players (game-state-helper)))
        "Gets players from game-state.")

    (is (nil? (g/game->players :anything))
        "Returns nil when game not found.")))

(deftest marker-accessor
  (testing "Testing get-player-marker"
    (is (thrown? Exception (g/get-player-marker :anything :anything))
        "Throws if service is not running.")

    (testing "Gets player marker with game-uid and player-uid."
      (are [expected result]
        (= expected result)
        :x (let [game (game-state-helper)
                 [[_ player] _] (g/game->players game)]
             (g/get-player-marker game player))

        :o (let [game (game-state-helper)
                 [_ [_ player]] (g/game->players game)]
             (g/get-player-marker game player))))

    (is (nil? (g/get-player-marker (game-state-helper) :anything))
        "Returns nil when player-uid not found in game-state.")

    (is (nil? (g/get-player-marker :anything :anything))
        "Returns nil when game-uid is not found.")))

(deftest make-move
  (testing "Testing make-move!"
    (is (thrown? Exception (g/make-move! :anything []))
        "Throws if service is not running.")

    ;; Simulate several moves and assert on intermediate and final states.
    (let [game-uid (game-state-helper)
          {new-state-1 game-uid} (g/make-move! game-uid [[0 :x] [1 :_] [2 :_]
                                                         [3 :_] [4 :_] [5 :_]
                                                         [6 :_] [7 :_] [8 :_]])
          {new-state-2 game-uid} (g/make-move! game-uid [[0 :x] [1 :_] [2 :_]
                                                         [3 :o] [4 :_] [5 :_]
                                                         [6 :_] [7 :_] [8 :_]])
          {new-state-3 game-uid} (g/make-move! game-uid [[0 :x] [1 :_] [2 :_]
                                                         [3 :o] [4 :x] [5 :_]
                                                         [6 :_] [7 :_] [8 :_]])]
      (are [expected result]
        (= expected result)
        :o (:turn new-state-1)
        :x (:turn new-state-2)
        :o (:turn new-state-3)
        :o (-> @state game-uid :turn)

        (-> @state game-uid :board) (:board new-state-3)))))
