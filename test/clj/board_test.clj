(ns clj.board-test
  (:require
    [clojure.test :refer :all]
    [tic-tac-toe.board :as b]))

(deftest board-generation
  (is (= [[0 :_] [1 :_] [2 :_]
          [3 :_] [4 :_] [5 :_]
          [6 :_] [7 :_] [8 :_]] b/empty-board)
      "Generates basic board."))

(deftest generate-next
  (testing "Testing generate-next"
    (let [board [[0 :_] [1 :x]
                 [2 :o] [3 :_]]]
      (testing "Valid inputs"
        (are [expected generated-next]
          (= expected generated-next)
          [[0 :x] [1 :x]
           [2 :o] [3 :_]] (b/generate-next board 0 :x)
          [[0 :_] [1 :x]
           [2 :o] [3 :_]] (b/generate-next board 0 :_)
          [[0 :_] [1 :o]
           [2 :o] [3 :_]] (b/generate-next board 1 :o)))

      (testing "Out of bounds inputs"
        (is (thrown? AssertionError (b/generate-next board 5 :x))
            "Throws when placing after board.")

        (is (thrown? AssertionError (b/generate-next board -2 :x))
            "Throws when placing before board.")))))

(deftest valid-move?
  (testing "Testing valid-move?"
    (testing "Allowed moves"
      (are [result]
        (true? result)
        (b/valid-move? [[0 :_] [1 :_]]
                       [[0 :x] [1 :_]]
                       :x)

        (b/valid-move? [[0 :_] [1 :_]]
                       [[0 :_] [1 :x]]
                       :x)

        (b/valid-move? [[0 :x] [1 :_]]
                       [[0 :x] [1 :o]]
                       :o)

        (b/valid-move? [[0 :_] [1 :o]]
                       [[0 :x] [1 :o]]
                       :x)))

    (testing "Illegal moves"
      (are [result]
        (false? result)
        (b/valid-move? [[0 :_] [1 :_]]
                       [[0 :_] [1 :_]]
                       :x)

        (b/valid-move? [[0 :_] [1 :_]]
                       [[0 :x] [1 :x]]
                       :x)

        (b/valid-move? [[0 :_] [1 :_]]
                       [[0 :o] [1 :_]]
                       :x)

        (b/valid-move? [[0 :x] [1 :_]]
                       [[0 :_] [1 :_]]
                       :x)

        (b/valid-move? [[0 :_] [1 :_]]
                       [[1 :_] [0 :_]]
                       :x)))

    (is (thrown? AssertionError (b/valid-move? [[0 :_] [1 :_]]
                                               [[0 :_]]
                                               :o))
        "Throws on reducing number of board elements.")

    (is (thrown? AssertionError (b/valid-move? [[0 :_] [1 :_]]
                                               [[0 :_] [1 :_] [2 :_]]
                                               :o))
        "Throws on increasing number of board elements.")))

(deftest finished?
  (testing "Testing finished?"
    (testing "Finished boards"
      (testing "Wins"
        (are [expected result]
          (= expected result)
          :x (b/finished? [[0 :x] [1 :x] [2 :x]
                           [3 :_] [4 :_] [5 :_]
                           [6 :_] [7 :_] [8 :_]])

          :x (b/finished? [[0 :x] [1 :_] [2 :_]
                           [3 :x] [4 :_] [5 :_]
                           [6 :x] [7 :_] [8 :_]])

          :x (b/finished? [[0 :x] [1 :_] [2 :_]
                           [3 :_] [4 :x] [5 :_]
                           [6 :_] [7 :_] [8 :x]])

          :o (b/finished? [[0 :o] [1 :o] [2 :o]
                           [3 :_] [4 :x] [5 :_]
                           [6 :_] [7 :_] [8 :x]])

          :o (b/finished? [[0 :o] [1 :o] [2 :o]
                           [3 :o] [4 :x] [5 :x]
                           [6 :x] [7 :x] [8 :o]])))

      (testing "Ties"
        (are [result]
          (= :tie result)
          (b/finished? [[0 :x] [1 :o] [2 :x]
                        [3 :x] [4 :x] [5 :o]
                        [6 :o] [7 :x] [8 :o]])

          (b/finished? [[0 :x] [1 :o] [2 :x]
                        [3 :o] [4 :x] [5 :o]
                        [6 :o] [7 :x] [8 :o]]))))

    (testing "Unfinished boards"
      (are [result]
        (nil? result)
        (b/finished? [[0 :_] [1 :_] [2 :_]
                      [3 :_] [4 :_] [5 :_]
                      [6 :_] [7 :_] [8 :_]])

        (b/finished? [[0 :x] [1 :_] [2 :_]
                      [3 :_] [4 :_] [5 :_]
                      [6 :_] [7 :_] [8 :_]])

        (b/finished? [[0 :x] [1 :_] [2 :_]
                      [3 :_] [4 :_] [5 :_]
                      [6 :_] [7 :_] [8 :_]])

        (b/finished? [[0 :x] [1 :x] [2 :_]
                      [3 :_] [4 :_] [5 :_]
                      [6 :_] [7 :_] [8 :_]])

        (b/finished? [[0 :x] [1 :_] [2 :_]
                      [3 :_] [4 :_] [5 :_]
                      [6 :_] [7 :_] [8 :x]])

        (b/finished? [[0 :x] [1 :o] [2 :o]
                      [3 :x] [4 :o] [5 :o]
                      [6 :_] [7 :x] [8 :x]])))))
