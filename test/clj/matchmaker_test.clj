(ns clj.matchmaker-test
  (:require
    [clojure.test :refer :all]
    [tic-tac-toe.matchmaker :as m]
    [tic-tac-toe.game :as game]
    [clojure.core.async :as a]
    [tic-tac-toe.util :as util])
  (:import (clojure.core.async.impl.channels ManyToManyChannel)))

(defonce ^:private in-ch @#'m/in-ch)

(use-fixtures :each util/spy-fixture)

(deftest matchmaker-lifecycle
  (testing "Testing start! & stop!"
    (with-redefs [m/start-listener! util/spy-fn]
      (let [send-fn (constantly nil)]
        (is (and (instance? ManyToManyChannel (do (m/start! send-fn) @in-ch))
                 (= (first @util/spy-atom) (list @in-ch send-fn #'m/process-players!)))
            "Start initialises state atom and starts listener.")))

    (is (nil? (do (m/stop!) @in-ch)) "Stop resets state atom.")))

(deftest enlisting-players
  (testing "Testing enlist-player!"
    (is (thrown? Exception (m/enlist-player! :anything))
        "Throws error if service not started.")

    (with-redefs [m/start-listener! (constantly nil)]
      (do (m/start! nil)
          (m/enlist-player! :player)
          (is (= (a/<!! @in-ch) :player)
              "Places player in the matchmaking queue.")))))

(deftest processing-players
  (testing "Testing process-players! (private)"
    (testing "Players are messaged when a match is made."
      (with-redefs [game/init-game! (constantly 789)]
        (do (@#'m/process-players! {:uid 123} {:uid 456} util/spy-fn)
            (let [[[player-uid-1 [event-id-1 [_ game-uid-1]]]
                   [player-uid-2 [event-id-2 [_ game-uid-2]]]] @util/spy-atom]
              (are [expected result]
                (= expected result)
                456 player-uid-1
                123 player-uid-2)

              (are [expected result1 result2]
                (= expected result1 result2)
                :game/opponent-found event-id-1 event-id-2
                789 game-uid-1 game-uid-2)))))))
