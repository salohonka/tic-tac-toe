(ns clj.websocket.core-test
  (:require
    [clojure.test :refer :all]
    [tic-tac-toe.websocket.core :as ws]
    [tic-tac-toe.websocket.message-handler :as msg-handler]
    [tic-tac-toe.util :as util]))

(defonce server @#'ws/ws-server)

(use-fixtures :each util/spy-fixture)

(deftest ws-lifecycle
  (testing "Testing start! & stop!"
    (testing "Start initialises state atom and starts listener."
      (with-redefs [ws/start-listener! util/spy-fn]
        (do (ws/start!)
            (is (map? @server))
            (is (= (first @util/spy-atom)
                   (list (:ch-recv @server) #'ws/handle-message!))))))

    (is (nil? (do (ws/stop!) @server)) "Stop reset state atom.")))

(deftest get-handler
  (testing "Testing get-handler"
    (is (thrown? Exception (ws/get-handler :anything))
        "Throws if service is not running.")

    (do (reset! server {:ajax-get-or-ws-handshake-fn (constantly :get-response)})
        (is (= :get-response (ws/get-handler :anything))
            "Uses get-handler from server state."))))

(deftest post-handler
  (testing "Testing post-handler"
    (is (thrown? Exception (ws/post-handler :anything))
        "Throws if service is not running.")

    (reset! server {:ajax-post-fn (constantly :post-response)})
    (is (= :post-response (ws/post-handler :anything))
        "Uses post-handler from server state.")))

(deftest send-fn
  (testing "Testing send!"
    (is (thrown? Exception (ws/send! :user :event))
        "Throws is service is not running.")

    (do (reset! server {:send-fn (constantly :send-ack)})
        (is (= :send-ack (ws/send! :user :event))
            "Uses send fn from server state."))))

(deftest ws-message-handler
  (testing "Testing handle-message!"
    (let [send-fn (constantly nil)]
      (is (nil? (@#'ws/handle-message! {:id :chsk/anything} send-fn))
          "Doesn't process sente's own events")

      (with-redefs [msg-handler/handle! util/spy-fn]
        (do (@#'ws/handle-message! {:id    :game/anything
                                    :?data "something"} send-fn)
            (is (= (first @util/spy-atom)
                   (list {:id    :game/anything
                          :?data "something"}
                         send-fn))
                "Forwards custom ns message to message-handler multimethod"))))))
