(ns clj.websocket.message-handler-test
  (:require
    [clojure.test :refer :all]
    [tic-tac-toe.websocket.message-handler :as mh]
    [tic-tac-toe.matchmaker :as mm]
    [tic-tac-toe.game :as game]
    [tic-tac-toe.board :as board]
    [tic-tac-toe.util :as util]))

(use-fixtures :each util/spy-fixture)

(deftest find-opponent-handler
  (with-redefs [mm/enlist-player! util/spy-fn]
    (do (mh/handle! {:id    :game/find-opponent
                     :?data :anything}
                    nil)
        (is (= {:id    :game/find-opponent
                :?data :anything}
               (ffirst @util/spy-atom))
            "Find-opponent event enlists player in matchmaking."))))


(deftest board-click-handler
  (testing "Testing board-click handling"
    (testing "Legal, non-ending move is updated to game-state and sent to players."
      (with-redefs [game/get-game (constantly {:board   board/empty-board
                                               :turn    :x
                                               :player1 [:x :player1-uid]
                                               :player2 [:o :player2-uid]})
                    game/get-player-marker (constantly :x)
                    game/make-move! (constantly nil)]
        (do (mh/handle! {:id    :game/board-click
                         :?data [0 :game-uid]
                         :uid   :player1-uid}
                        util/spy-fn)
            (are [expected result]
              (= expected result)
              :player2-uid (ffirst @util/spy-atom)
              :player1-uid (-> @util/spy-atom second first))

            (is (= [:game/move-confirmed (assoc-in board/empty-board [0 1] :x)]
                   (-> @util/spy-atom first second)
                   (-> @util/spy-atom second second))))))

    (testing "Legal, ending move is accepted, and game-ended sent to players."
      (with-redefs [game/get-game (constantly {:board   [[0 :_] [0 :o] [0 :_]
                                                         [0 :x] [0 :o] [0 :_]
                                                         [0 :x] [0 :_] [0 :_]]
                                               :turn    :x
                                               :player1 [:x :player1-uid]
                                               :player2 [:o :player2-uid]})
                    game/get-player-marker (constantly :x)
                    game/make-move! (constantly nil)
                    game/close-game! util/spy-fn]
        (do (reset! util/spy-atom nil)                      ; Fixtures are `deftest` level, clean up from last assert.
            (mh/handle! {:id    :game/board-click
                         :?data [0 :game-uid]
                         :uid   :player1-uid}
                        util/spy-fn)
            (are [expected x y]
              (= expected x y)
              :player2-uid
              (-> @util/spy-atom (nth 1) first)
              (-> @util/spy-atom (nth 2) first)

              :player1-uid
              (-> @util/spy-atom (nth 3) first)
              (-> @util/spy-atom (nth 4) first)

              [:game/game-ended :x]
              (-> @util/spy-atom (nth 1) second)
              (-> @util/spy-atom (nth 3) second))
            (is (= :game-uid (ffirst @util/spy-atom))))))

    (with-redefs [game/get-game (constantly {:board [[0 :_] [1 :x]]
                                             :turn  :o})
                  game/get-player-marker (constantly :x)]
      (do (reset! util/spy-atom nil)
          (mh/handle! {:id    :game/board-click
                       :?data [1 :game-uid]
                       :uid   :player-uid}
                      util/spy-fn)
          (is (= (list :player-uid [:game/illegal-move])
                 (first @util/spy-atom))
              "Illegal move is disregarded and player notified.")))))

(deftest unrecognized-handler
  (is (thrown? Exception (mh/handle! {:id :anything} nil))
      "Throws on unrecognized message."))
