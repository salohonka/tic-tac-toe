(ns cljs.websocket.message-handler-test
  (:require
    [cljs.test :refer-macros [deftest is use-fixtures]]
    [tic-tac-toe.websocket.message-handler :as handler]
    [re-frame.core :as rf]
    [tic-tac-toe.util :as util]))

(use-fixtures :each util/spy-fixture)

(defn- dispatch-helper [incoming-event outbound-event description]
  (with-redefs [rf/dispatch util/spy-fn]
               (do (handler/handle! incoming-event :some-param)
                   (is (= [outbound-event :some-param]
                          (ffirst @util/spy-atom))
                       description))))

(deftest handle-opponent-found
  (dispatch-helper :game/opponent-found
                   :game-found
                   "Dispatches game found event with params"))

(deftest handle-move-confirmed
  (dispatch-helper :game/move-confirmed
                   :move-confirmed
                   "Dispatches move confirmed event with params"))

(deftest handle-game-ended
  (dispatch-helper :game/game-ended
                   :game-ended
                   "Dispatches game ended event with params"))
