(ns cljs.websocket.core-test
  (:require
    [tic-tac-toe.websocket.core :as ws]
    [cljs.test :refer-macros [deftest is use-fixtures]]
    [tic-tac-toe.util :as util]))

(use-fixtures :each util/spy-fixture)

(deftest send-fn
  (with-redefs
    [ws/get-client! (constantly {:send-fn util/spy-fn})]
    (ws/send! "arg1" "arg2" "arg3")
    (is (= '("arg1" "arg2" "arg3")
           (first @util/spy-atom))
        "Send calls send-fn from server object with args.")))
