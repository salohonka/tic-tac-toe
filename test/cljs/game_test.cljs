(ns cljs.game-test
  (:require
    [cljs.test :refer-macros [deftest is]]
    [tic-tac-toe.game :as game]))

(deftest board-generation
  (is (= [[0 :_] [1 :_] [2 :_]
          [3 :_] [4 :_] [5 :_]
          [6 :_] [7 :_] [8 :_]] game/empty-board)
      "Generates basic board"))
