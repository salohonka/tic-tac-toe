(ns cljs.state-machine-test
  (:require
    [cljs.test :refer-macros [deftest is]]
    [tic-tac-toe.state-machine :as sm]))

(deftest default-transitions
  (is (= {:state :looking-for-game}
         (sm/update {:state :win} :start-looking-for-game))
      "Valid transition returns updated state.")

  (is (= {:state :win}
         (sm/update {:state :win} :some-action))
      "Invalid transition returns unchanged state."))

(deftest game-ended-transition
  (is (= {:state :tie}
         (sm/update {:state :turns/my-turn} :game-ended :tie))
      "Game end during my turn with :tie results in :tie state.")

  (is (= {:state :lose}
         (sm/update {:state :turns/my-turn} :game-ended :anything))
      "Game end during my turn with anything except :tie results in :lose state.")

  (is (= {:state :tie}
         (sm/update {:state :turns/opponent-turn} :game-ended :tie))
      "Game end during opponent turn with :tie results in :tie state.")

  (is (= {:state :win}
         (sm/update {:state :turns/opponent-turn} :game-ended :anything))
      "Game end during opponent turn with anything except :tie results in :win state."))

(deftest game-found-transition
  (is (= {:state :turns/opponent-turn}
         (sm/update {:state :looking-for-game} :game-found :o))
      "Game found with :o param results in my turn state.")

  (is (= {:state :turns/my-turn}
         (sm/update {:state :looking-for-game} :game-found :anything))
      "Game found with anything but :o param results in my turn state."))
