(defproject tic-tac-toe "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.773"
                  :exclusions [com.google.javascript/closure-compiler-unshaded
                               org.clojure/google-closure-library
                               org.clojure/google-closure-library-third-party]]
                 [org.clojure/core.async "1.3.610"]
                 [thheller/shadow-cljs "2.11.7"]
                 [hiccup "1.0.5"]
                 [reagent "0.10.0"]
                 [re-frame "1.1.2"]
                 [metosin/reitit "0.5.10"]
                 [yogthos/config "1.1.7"]
                 [ring/ring-anti-forgery "1.3.0"
                  :exclusions [commons-codec]]
                 [ring "1.8.2"]
                 [http-kit "2.5.0"]
                 [com.taoensso/sente "1.16.0"]]

  :plugins [[lein-shadow "0.3.1"]
            [lein-less "1.7.5"]
            [lein-shell "0.5.0"]]

  :min-lein-version "2.9.0"

  :source-paths ["src/clj" "src/cljs" "src/cljc"]

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]

  :less {:source-paths ["less"]
         :target-path  "resources/public/css"}

  :shadow-cljs {:nrepl  {:port 8777}

                :builds {:app        {:target     :browser
                                      :output-dir "resources/public/js/compiled"
                                      :asset-path "/js/compiled"
                                      :modules    {:app {:init-fn  tic-tac-toe.core/init
                                                         :preloads [re-frisk.preload]}}}
                         :karma-test {:target    :karma
                                      :output-to "target/karma/test.js"
                                      :ns-regexp "-test$"}}}

  :shell {:commands {"karma" {:windows         ["cmd" "/c" "karma"]
                              :default-command "karma"}
                     "open"  {:windows ["cmd" "/c" "start"]
                              :macosx  "open"
                              :linux   "xdg-open"}}}

  :aliases {"dev"          ["do"
                            ["shell" "echo" "\"DEPRECATED: Please use lein watch instead.\""]
                            ["watch"]]
            "watch"        ["with-profile" "dev" "do"
                            ["shadow" "watch" "app" "browser-test" "karma-test"]]

            "prod"         ["do"
                            ["shell" "echo" "\"DEPRECATED: Please use lein release instead.\""]
                            ["release"]]

            "release"      ["with-profile" "prod" "do"
                            ["shadow" "release" "app"]]

            "build-report" ["with-profile" "prod" "do"
                            ["shadow" "run" "shadow.cljs.build-report" "app" "target/build-report.html"]
                            ["shell" "open" "target/build-report.html"]]

            "karma"        ["do"
                            ["shell" "echo" "\"DEPRECATED: Please use lein ci instead.\""]
                            ["ci"]]
            "ci"           ["with-profile" "prod" "do"
                            ["shadow" "compile" "karma-test"]
                            ["shell" "karma" "start" "--single-run" "--reporters" "junit,dots"]]}

  :profiles {:dev     {:dependencies   [[re-frisk "1.3.4"]]
                       :source-paths   ["dev"]
                       :resource-paths ["config/dev"]}

             :prod    {:source-paths   ["env/prod/clj"]
                       :resource-paths ["config/prod"]}

             :uberjar {:source-paths   ["env/prod/clj"]
                       :resource-paths ["config/prod"]
                       :omit-source    true
                       :main           tic-tac-toe.core
                       :aot            [tic-tac-toe.core]
                       :uberjar-name   "tic-tac-toe.jar"
                       :prep-tasks     ["compile" ["release"] ["less" "once"]]}}

  :prep-tasks [["less" "once"]])
