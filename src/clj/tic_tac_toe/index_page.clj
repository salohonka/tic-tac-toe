(ns tic-tac-toe.index-page
  (:require
    [hiccup.core :refer [html]]
    [ring.middleware.anti-forgery :as af]))

(defn- view []
  (html [:html {:lang "en"}
         [:head
          [:meta {:charset "utf-8"}]
          [:meta {:name "viewport", :content "width=device,initial-scale=1"}]
          [:link {:href "css/site.css", :rel "stylesheet", :type "text/css"}]
          [:link {:href "https://fonts.gstatic.com", :rel "preconnect"}]
          [:link {:href "https://fonts.googleapis.com/css2?family=Dosis:wght@700&display=swap"
                  :rel  "stylesheet"}]
          [:title "tic-tac-toe"]]
         [:body
          [:noscript "This app requires JavaScript."]
          [:div#sente-csrf-token {:data-csrf-token (force af/*anti-forgery-token*)}]
          [:div#app]
          [:script {:src "js/compiled/app.js"}]]]))

(defn handler [_] {:status 200, :body (view)})
