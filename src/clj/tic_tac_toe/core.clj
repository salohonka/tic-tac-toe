(ns tic-tac-toe.core
  (:require [tic-tac-toe.router :refer [handler dev-handler]]
            [config.core :refer [env]]
            [org.httpkit.server :refer [run-server server-stop!]]
            [tic-tac-toe.websocket.core :as ws]
            [tic-tac-toe.matchmaker :as mm]
            [tic-tac-toe.game :as game])
  (:gen-class))

(defonce ^:private web-server (atom nil))

(defn- stop! []
  (when-let [server @web-server]
    (server-stop! server))
  (reset! web-server nil))

(defn- start! []
  (stop!)
  (reset! web-server
          (let [port (or (:port env) 3000)
                ring-handler (if (:debug env) dev-handler handler)]
            (run-server ring-handler {:port                 port
                                      :legacy-return-value? false}))))

(defn -main [& _args]
  (let [{:keys [send-fn]} (ws/start!)]
    (game/start!)
    (mm/start! send-fn)
    (start!)))

(comment
  (-main))
