(ns tic-tac-toe.matchmaker
  (:require
    [clojure.core.async :as as :refer [chan >! >!! <!]]
    [tic-tac-toe.game :refer [init-game!]]))

(defonce ^:private in-ch (atom nil))

(defn enlist-player! [player]
  (if @in-ch (>!! @in-ch player)
             (throw (Exception. "Cannot enlist players when matchmaker is not running! Did you forget to `(start!)`?"))))

(defn- process-players! [player1 player2 send-fn]
  (let [players (->> [player1 player2]
                     (map :uid)
                     (map vector (shuffle [:x :o])))
        game-uid (init-game! (first players) (second players))]
    (doseq [[marker player-uid] players]
      (send-fn player-uid [:game/opponent-found [marker game-uid]]))))

(defn- start-listener! [in-ch send-fn message-handler]
  (as/go-loop []
    (let [player1 (<! in-ch)
          player2 (<! in-ch)]
      (message-handler player1 player2 send-fn)
      (when in-ch (recur)))))

(defn stop! []
  (when @in-ch (as/close! @in-ch))
  (reset! in-ch nil))

(defn start! [send-fn]
  (stop!)
  (let [ch (chan 10)]
    (reset! in-ch ch)
    (start-listener! ch send-fn #'process-players!)))
