(ns tic-tac-toe.game
  (:require
    [tic-tac-toe.board :as board])
  (:import (java.util UUID)))

(defonce ^:private state (atom nil))

(defn init-game! [player1 player2]
  (if @state (let [uid (keyword (.toString (UUID/randomUUID)))]
               (swap! state assoc uid {:board   board/empty-board
                                       :player1 player1
                                       :player2 player2
                                       :turn    :x})
               uid)
             (throw (Exception. "Cannot init game when game-module is not running! Did you forget to `(start!)`?"))))

(defn get-game [uid]
  (if @state (get @state uid)
             (throw (Exception. "Cannot get game when game-module is not running! Did you forget to `(start!)`?"))))

(defn close-game! [uid]
  (swap! state dissoc uid))

(defn game->players [game-uid]
  (-> (get-game game-uid) (select-keys [:player1 :player2]) vals))

(defn get-player-marker [game-uid player-uid]
  (->> game-uid
       game->players
       (some (fn [[marker uid]] (when (= player-uid uid) marker)))))

(defn make-move! [game-uid board]
  (let [game-state (get-game game-uid)
        next-turn (if (= :x (:turn game-state)) :o :x)
        updated-state (assoc game-state
                        :board board
                        :turn next-turn)]
    (swap! state assoc game-uid updated-state)))

;;; Lifecycle handlers

(defn stop! [] (reset! state nil))

(defn start! [] (reset! state {}))

(comment
  (start!)
  @state)
