(ns tic-tac-toe.websocket.core
  (:require
    [clojure.core.async :as a]
    [taoensso.sente :as sente]
    [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
    [taoensso.sente.server-adapters.http-kit :as http-kit]
    [tic-tac-toe.websocket.message-handler :as message-handler])
  (:import (java.util UUID)))

(defonce ^:private ws-server (atom nil))

(def not-started-error (Exception. "Websocket server is not running! Did you forget to `(start!)`?"))

;; Websocket interaction functions

(defn post-handler [req]
  (if-let [handler (:ajax-post-fn @ws-server)]
    (handler req)
    (throw not-started-error)))

(defn get-handler [req]
  (if-let [handler (:ajax-get-or-ws-handshake-fn @ws-server)]
    (handler req)
    (throw not-started-error)))

(defn send!
  "Event arg: [::event-id ?data]]"
  [user-id event]
  (if-let [send-fn (:send-fn @ws-server)]
    (send-fn user-id event)
    (throw not-started-error)))

;; Incoming message listener

(defn- handle-message! [message send-fn]
  (let [{:keys [id] :as msg} message
        custom-ns (not= "chsk" (namespace id))]
    (when custom-ns
      (message-handler/handle! msg send-fn))))

(defn- start-listener! [receive-ch message-handler]
  (a/go-loop []
    (message-handler (a/<! receive-ch) send!)
    (when @ws-server (recur))))

;; Lifecycle

(defn stop! []
  (when-let [chsk (:chsk @ws-server)]
    (sente/chsk-disconnect! chsk))
  (reset! ws-server nil))

(defn start! []
  (stop!)
  (let [server (sente/make-channel-socket-server!
                 (http-kit/get-sch-adapter)
                 {:user-id-fn (fn [_] (.toString (UUID/randomUUID)))})]
    (reset! ws-server server)
    (start-listener! (:ch-recv server) #'handle-message!)
    server))

(comment
  (-> @ws-server :connected-uids))
