(ns tic-tac-toe.websocket.message-handler
  (:require
    [tic-tac-toe.matchmaker :as mm]
    [tic-tac-toe.game :as game]
    [tic-tac-toe.board :as board]))

(defn- handle-move [game-uid next-board send-fn]
  (let [game-result (board/finished? next-board)]
    (game/make-move! game-uid next-board)
    (doseq [[_ player-uid] (game/game->players game-uid)]
      (send-fn player-uid [:game/move-confirmed next-board])
      (when game-result (send-fn player-uid [:game/game-ended game-result])))
    (when game-result (game/close-game! game-uid))))

(defmulti handle!
          "Processes incoming game related websocket messages."
          (fn [{:keys [id]} _] id))

(defmethod handle! :game/find-opponent [msg _]
  (mm/enlist-player! msg))

(defmethod handle! :game/board-click [{:keys [?data uid]} send-fn]
  (let [[position game-uid] ?data
        {:keys [board turn]} (game/get-game game-uid)
        next-board (board/generate-next board
                                        position
                                        (game/get-player-marker game-uid uid))]
    (if (board/valid-move? board next-board turn)
      (handle-move game-uid next-board send-fn)
      (send-fn uid [:game/illegal-move]))))
