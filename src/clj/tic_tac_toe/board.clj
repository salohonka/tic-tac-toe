(ns tic-tac-toe.board
  (:require
    [clojure.data :as data]))

(def grid-size 3)

(def empty-board
  (->> (* grid-size grid-size)
       range
       (mapv (fn [i] [i :_]))))

(defn- diff
  "Returns vec of two collections: `[old-markers new-markers]`,
  where the two collections map the diffs in state. This function
  does not validate the diffs! Use `valid-move?` to check if move
  is legal.

  E.g. `[(:_ :o) (:o :x)]` means two markers have changed, one
  blank into a circle and one circle into a cross."
  [prev-board next-board]
  (let [diffs (data/diff prev-board next-board)
        new-changes (->> diffs second flatten (remove nil?))
        changed-from (->> diffs first flatten (remove nil?))]
    [changed-from new-changes]))

(defn generate-next
  "Inserts `marker` to `position` in `board`.
  This function does not validate the generated board state!
  Use `valid-move?` to check if move is legal."
  [board position marker]
  {:pre [(<= 0 position (dec (count board)))]}
  (assoc board position [position marker]))

(defn valid-move?
  "Validates a new board state against an existing board state.
  Checks if the move was made on the players turn, on an empty tile,
  and that only one tile was changed."
  [previous-board next-board turn]
  {:pre [(= (count previous-board) (count next-board))]}
  (let [[from to] (diff previous-board next-board)]
    (and (= 1 (count from))
         (= :_ (first from))
         (= turn (first to)))))

(defn- winning-line?
  "Is collection [[position :marker]+] all the same :marker"
  [line]
  (let [markers (->> line (map second))]
    (when (and (not= (first markers) :_) (apply = markers))
      (-> line first second))))

(defn- tied? [board]
  (when (->> (map second board) flatten (filter #{:_}) empty?) :tie))

(defn- group-by-rows [board]
  (partition-all grid-size board))

(defn- group-by-columns [board]
  (for [offset (range grid-size)]
    (take-nth grid-size (drop offset board))))

(defn- group-by-diagonals [board]
  (list (take-nth (inc grid-size) board)
        (->> board (drop (dec grid-size)) (drop-last (dec grid-size)) (take-nth (dec grid-size)))))

(defn finished?
  "Checks if board is finished. Returns the winning marker (:o or :x)
  or :tie if no moves can be played. Nil in case the board is not
  finished and possible moves remain."
  [board]
  (let [groups ((juxt group-by-rows
                      group-by-columns
                      group-by-diagonals) board)]
    (or (->> (for [group groups, line group] (winning-line? line))
             (filter #{:x :o})
             first)
        (tied? board))))
