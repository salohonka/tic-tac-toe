(ns tic-tac-toe.router
  (:require
    [reitit.ring :as r]
    [ring.middleware.reload :refer [wrap-reload]]
    [ring.middleware.keyword-params :refer [wrap-keyword-params]]
    [ring.middleware.params :refer [wrap-params]]
    [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
    [ring.middleware.session :refer [wrap-session]]
    [ring.middleware.stacktrace :as trace]
    [tic-tac-toe.websocket.core :as ws]
    [tic-tac-toe.index-page :as index]))

(def handler
  (r/ring-handler
    (r/router
      [["/ws" {:get ws/get-handler, :post ws/post-handler}]
       ["/ping" {:get (constantly {:status 200, :body "pong"})}]
       ["/" {:get index/handler}]])
    (r/routes
      (r/create-resource-handler {:path "/"})
      (r/create-default-handler))
    {:middleware [wrap-session wrap-anti-forgery wrap-params wrap-keyword-params]}))

(def dev-handler (-> #'handler wrap-reload trace/wrap-stacktrace))
