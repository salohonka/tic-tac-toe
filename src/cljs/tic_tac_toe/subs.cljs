(ns tic-tac-toe.subs
  (:require
    [re-frame.core :as rf]))

(rf/reg-sub :ws-client (fn [db] (-> db :ws :client)))

(rf/reg-sub :ws-open? (fn [db] (-> db :ws :open)))

(rf/reg-sub :state (fn [db] (:state db)))

(rf/reg-sub :board (fn [db] (:board db)))

(rf/reg-sub :my-turn? :<- [:state] (fn [state _] (= state :turns/my-turn)))

(rf/reg-sub :marker (fn [db] (:marker db)))

(rf/reg-sub :game-uid (fn [db] (:game-uid db)))

(rf/reg-sub
  :game-over? :<- [:state]
  (fn [state _] (some #{state} [:win :lose :tie])))

(rf/reg-sub
  :status-text :<- [:state]
  (fn [state _]
    (case state
      :tie "Game tied."
      :win "You won!"
      :lose "You lost."
      :turns/opponent-turn "Opponent's turn..."
      :turns/my-turn "Your turn!"
      nil)))
