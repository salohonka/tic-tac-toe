(ns tic-tac-toe.events
  (:require
    [re-frame.core :as rf]
    [tic-tac-toe.db :as db]
    [tic-tac-toe.state-machine :as fsm]
    [tic-tac-toe.game :as game]))

;;; Basic lifecycle events

(rf/reg-event-db
  :initialize-db
  (fn [_ _]
    db/default-db))

(rf/reg-event-fx
  :close-ws
  (fn [{:keys [db]} _]
    {:db (assoc-in db [:ws :client] nil)
     :ws [:close]}))

(rf/reg-event-fx
  :open-ws
  (fn [{:keys [db]} _]
    {:db (fsm/update db :try-to-connect)
     :ws [:open :ws-opened]}))

(rf/reg-event-db
  :ws-opened
  (fn [db [_ client]]
    (assoc-in db [:ws :client] client)))

;;; Websocket interaction events

(rf/reg-event-fx
  :set-ws-connection-status
  (fn [{:keys [db]} [_ is-open]]
    (let [updated-db (assoc-in db [:ws :open] is-open)]
      (if is-open
        {:db (fsm/update updated-db :connection-made)
         ;; If connection is opened, start looking for opponent
         :ws [:game/find-opponent]}
        {:db updated-db}))))

(rf/reg-event-db
  :game-found
  (fn [db [_ [marker game-uid]]]
    (merge (fsm/update db :game-found marker)
           {:board game/empty-board, :marker marker, :game-uid game-uid})))

(rf/reg-event-fx
  :board-click
  (fn [_ [_ [position _] game-uid]]
    {:ws [:game/board-click [position game-uid]]}))

(rf/reg-event-db
  :move-confirmed
  (fn [db [_ board]]
    (merge (fsm/update db :move-confirmed)
           {:board board})))

(rf/reg-event-db
  :game-ended
  (fn [db [_ game-result]]
    (fsm/update db :game-ended game-result)))

(rf/reg-event-fx
  :find-new-game
  (fn [{:keys [db]} _]
    {:db (fsm/update db :start-looking-for-game)
     :ws [:game/find-opponent]}))
