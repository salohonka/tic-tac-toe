(ns tic-tac-toe.websocket.core
  (:require-macros
    [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [cljs.core.async :refer [<!]]
    [taoensso.sente :as sente :refer [cb-success?]]
    [re-frame.core :as rf]
    [tic-tac-toe.websocket.message-handler :as message-handler]))

(defn- get-client! [] @(rf/subscribe [:ws-client]))

(def ?csrf-token
  (when-let [el (.getElementById js/document "sente-csrf-token")]
    (.getAttribute el "data-csrf-token")))

(defn- send!
  "[[::event-id ?data] & [?timeout-ms ?cb-fn]]"
  [& args]
  (when-let [send-fn (:send-fn (get-client!))]
    (apply send-fn args)))

;;; Incoming message listener

(defn- start-listener! [receive-ch message-handler]
  (go-loop [] (let [{:keys [?data] :as msg} (<! receive-ch)
                    message-type (first ?data)
                    custom-ns (and (keyword? message-type)
                                   (not= :chsk message-type))]
                (when msg
                  (when custom-ns (apply message-handler ?data))
                  (recur)))))

;;; Lifecycle

(defn- state-updater [_key _ref _old new]
  (rf/dispatch [:set-ws-connection-status (:open? new)]))

(defn- open! [cb-event-kw]
  (let [client (sente/make-channel-socket-client! "/ws" ?csrf-token)
        in-ch (:ch-recv client)]
    ;; Websocket connection state is in a nested atom, add watcher to utilize with re-frame
    (add-watch (:state client) :open-state state-updater)
    (start-listener! in-ch #'message-handler/handle!)
    (rf/dispatch [cb-event-kw client])))

(defn- close! []
  (sente/chsk-disconnect! (:chsk @(rf/subscribe [:ws-client]))))

;;; Effect handler

(rf/reg-fx
  :ws
  (fn [[action data]]
    (case action
      :open (open! data)
      :close (close!)
      (send! [action data]))))
