(ns tic-tac-toe.websocket.message-handler
  (:require
    [re-frame.core :as rf]))

(defmulti handle!
          "Processes incoming game related websocket messages.
          Args: `[message send-fn]`"
          (fn [type _] type))

(defmethod handle! :game/opponent-found [_ data]
  (rf/dispatch [:game-found data]))

(defmethod handle! :game/move-confirmed [_ board]
  (rf/dispatch [:move-confirmed board]))

(defmethod handle! :game/illegal-move [_ _]
  (.warn js/console "Illegal move made!"))

(defmethod handle! :game/game-ended [_ game-result]
  (rf/dispatch [:game-ended game-result]))

(defmethod handle! :default [type _]
  (.error js/console (str "Received unrecognized message: " type)))
