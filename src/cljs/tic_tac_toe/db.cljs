(ns tic-tac-toe.db)

(def default-db
  {:ws       {:client nil
              ;; Just having a client does not guarantee a connection exists yet
              :open   false}
   :state    :start
   :board    nil
   :marker   nil
   :game-uid nil})
