(ns tic-tac-toe.state-machine
  (:refer-clojure :exclude [update]))

(def transitions
  "`{:state-key {:action-key [:valid-next-states]}}`

  The win and lose states triggered by :game-ended are
  reversed, because backend always sends a
  :move-confirmed before :game-ended to sync the board
  a final time before game ends. This flips the turn a
  final time before game ending."
  {:start                  {:try-to-connect [:waiting-for-connection]}
   :waiting-for-connection {:connection-made [:looking-for-game]}
   :looking-for-game       {:game-found [:turns/my-turn :turns/opponent-turn]}
   :turns/my-turn          {:move-confirmed [:turns/opponent-turn]
                            :game-ended     [:lose :tie]}
   :turns/opponent-turn    {:move-confirmed [:turns/my-turn]
                            :game-ended     [:win :tie]}
   :win                    {:start-looking-for-game [:looking-for-game]}
   :lose                   {:start-looking-for-game [:looking-for-game]}
   :tie                    {:start-looking-for-game [:looking-for-game]}})

(derive :turns/my-turn :turns/turn)
(derive :turns/opponent-turn :turns/turn)

(defmulti ^:private transition (fn [state action & _params] [state action]))

(defmethod transition [:turns/turn :game-ended] [state action [result]]
  ((if (= result :tie) second first) (-> transitions state action)))

(defmethod transition [:looking-for-game :game-found] [state action [marker]]
  ((if (= marker :o) second first) (-> transitions state action)))

(defmethod transition :default [state action _]
  (if-let [next-state (-> transitions state action first)]
    next-state
    (throw (js/Error. (str "Unexpected action " action " in state " state)))))

(defn update
  "Updates the state key in `db` according to valid transitions."
  [db action & data]
  (try
    (clojure.core/update db :state transition action data)
    (catch :default e
      (.error js/console e)
      db)))
