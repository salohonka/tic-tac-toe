(ns tic-tac-toe.views.board
  (:require
    [re-frame.core :as rf]))

(def marker->label {:x "x", :o "o"})

(defn panel []
  (let [board @(rf/subscribe [:board])
        my-turn? @(rf/subscribe [:my-turn?])
        game-uid @(rf/subscribe [:game-uid])
        game-over? @(rf/subscribe [:game-over?])]
    [:div.board
     (for [[index marker] board]
       ^{:key index}
       [:button
        {:class    [(get marker->label marker)
                    (when-not game-over? "hoverable")]
         :disabled (not my-turn?)
         :on-click #(rf/dispatch [:board-click [index marker] game-uid])}
        [:span.button-label (get marker->label marker)]])]))
