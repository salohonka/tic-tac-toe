(ns tic-tac-toe.views.main
  (:require
    [re-frame.core :as rf]
    [tic-tac-toe.views.starting-screen :as starting-screen]
    [tic-tac-toe.views.loading :as loading]
    [tic-tac-toe.views.board :as board]
    [tic-tac-toe.views.status :as status]))

(defn main-panel []
  (let [state @(rf/subscribe [:state])
        board-data @(rf/subscribe [:board])]
    [:<>
     [:h1 [:span.tic "Tic"] " " [:span.tac "Tac"] " " [:span.toe "Toe"]]
     (case state
       :start [starting-screen/panel]
       :waiting-for-connection [loading/panel]
       :looking-for-game [loading/panel]
       [:<>
        [board/panel board-data]
        [status/panel]])]))
