(ns tic-tac-toe.views.starting-screen
  (:require
    [re-frame.core :as rf]))

(defn panel []
  [:button.start.hoverable
   {:on-click #(rf/dispatch [:open-ws])}
   "Find a game!"])
