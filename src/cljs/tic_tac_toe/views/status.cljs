(ns tic-tac-toe.views.status
  (:require
    [re-frame.core :as rf]))

(defn panel []
  (let [status-text @(rf/subscribe [:status-text])
        game-over? @(rf/subscribe [:game-over?])]
    [:div.status
     (when status-text [:h3 status-text])
     (when game-over? [:button.hoverable.another-game
                       {:on-click #(rf/dispatch [:find-new-game])}
                       "Find another game?"])]))
