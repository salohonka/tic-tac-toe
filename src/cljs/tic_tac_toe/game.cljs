(ns tic-tac-toe.game)

(def grid-size 3)

(def empty-board
  (->> (* grid-size grid-size)
       range
       (mapv (fn [i] [i :_]))))
