(ns tic-tac-toe.util)

(defonce spy-atom (atom nil))

(defn spy-fn [& args] (swap! spy-atom conj args))

(defn spy-fixture [f] (f) (reset! spy-atom nil))
